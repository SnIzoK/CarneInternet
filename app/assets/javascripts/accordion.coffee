$document.ready ->
    $('.accordion-button').first().addClass('opened')
    $('.accordion-inner').first().slideToggle(350)

$document.on 'click', '.accordion-button', (e)->

    e.preventDefault()
    
    $this = $(this)

    if $this.hasClass('opened')
        $this.removeClass('opened')
        $this.next().removeClass('show')
        $this.next().slideUp(350)
    else
        $this.addClass('opened')
        $this.next().addClass('show')
        $this.next().slideToggle(350)