Rails.application.routes.draw do
  root to: "pages#index"

  controller :pages do
    get "/profile-settings", action: "profile_settings"
  end

  get "/profile-settings" => "pages#profile_settings"
  get "/password-account" => "pages#password_account"
  get "/office-life-account" => "pages#office_life_account"
  get "/updates-account" => "pages#updates_account"
  get "/notification-account" => "pages#notification_account"
  get "/events-account" => "pages#events_account"


  get "/dashboard-page" => "pages#dashboard_page"
  get "/agenda" => "pages#agenda"
  get "/teem-page" => "pages#teem_page"
  get "/office-life-page" => "pages#office_life_page"
  get "/article-one" => "pages#article_one_page"
  get "/updates-page" => "pages#updates_page"


  match "*url", to: "application#render_not_found", via: [:get, :post, :path, :put, :update, :delete]
end