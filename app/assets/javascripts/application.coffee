#= require jquery
#= require jquery-ui
#= require jquery_ujs

#= require global

#     P L U G I N S

#= require plugins/jquery.validate.min
#= require plugins/jquery-easing
#= require plugins/jquery.appear
#= require plugins/clickout
#= require plugins/datepick
#= require plugins/form
#= require plugins/jquery.bxslider
#= require plugins/jquery.scrolldelta
#= require plugins/lightgallery.min
# require plugins/scroll-banner
#= require plugins/selectize.min
#= require plugins/parallax.min
# require plugins/masonry-docs.min
# require plugins/masonry.pkgd.min
#= require plugins/jquery.simple-calendar
#= require plugins/jquery.nice-select
#= require plugins/jquery.nice-select.min
#= require plugins/nc


#     I N I T I A L I Z E

#= require google_map
#= require appear-initialize
#= require bxslider
#= require fullpage_banner_height
#= require header
#= require characters_limit
#= require menu
#= require accordion
#= require selectize-initialize
#= require popups
#= require tabs
#= require navigation
#= require links
# require masonry
#= require main