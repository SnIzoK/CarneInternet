    ##          S W E E C H E  R        ##

$document.on 'click', '.sweecher', ()->
  if !$(this).hasClass("disabled")
    $(this).toggleClass('active')
  else
    $('.allerts-popup[data-popup-attr="error-massage"]').addClass("visible")
    $('body').addClass("opened-popup")

    ##          P A G I NA T I O N      ##

$document.on 'click', '.pagination-tab', ()->
  $(this).addClass('active')
  $(this).siblings().removeClass('active')

$document.on 'click', '.prev-button', ()->
  find('.pagination-tab .active').removeClass('active')


    ##       A G E N D A     E V E N T S    ##

$document.on 'click', '.one-event-box', ()->
  $(this).find('.short-info-event').toggleClass('visible')
  $(this).find('.name-event').toggleClass('opened')
  

    ##        I N P U T    H E A E R    ## 

$document.on 'click', '.search-button', ()->
  $('.one-page').addClass('hidden')
  $('.input-serch-header').addClass('visible')

  $.clickOut("#header-serch",
  ()->
    $('.one-page').removeClass('hidden')
    $('.input-serch-header').removeClass('visible')
  {except: '#header-serch, .search-button'}
  )



    ##  D E L E T E    A C C O U N T   U P D A T E

$document.on 'click', '.delete-tr', ->
  $('.content-table.target').remove()
  $('body').removeClass('opened-popup')
  $('.allerts-popup').removeClass('visible')

$document.on 'click', '.content-table', ->
  $(this).siblings().removeClass('target')
  $(this).addClass('target')

  

   ##  I N D E X   F O R M   S I N G    I N   ##

$document.on 'click', '.forgot-passwrd-button', ()->
  $('.return-page').removeClass('disn')
  $('form.sign-in').addClass('disn')
  $('form.forgot-passwrd').addClass('visible')

$document.on 'click', '.return-page', ()->
  $('.return-page').addClass('disn')
  $('form.sign-in').removeClass('disn')
  $('form.forgot-passwrd').removeClass('visible')


  ## A C T I V I T Y    D A S H B O R D

$document.on 'click', '.activity-filters-button', ()->
  $('.drop-down-filter-activity').addClass('visible')
  $.clickOut("drop-down-filter-activity",
  ()->
    $('.drop-down-filter-activity').removeClass('visible')
  {except: '.activity-filters-button'}
  )