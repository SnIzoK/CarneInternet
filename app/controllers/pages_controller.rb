class PagesController < ApplicationController

  def index
    @index = true
  end

  def dashboard_page
    @dashboard_page = true
    
  end

  def profile_settings
    @my_account = true
  end

  def password_account
    @my_account = true
  end

  def notification_account
    @my_account = true
  end

  def events_account
    @my_account = true
  end

  def updates_account
    @my_account = true
  end

  def office_life_account
    @my_account = true
  end

  def agenda
    @agenda = true
  end

  def dashbord_page
    @dashbord_page = true
  end

  def office_life_page
    @office_life_page = true
  end

  def teem_page 
    @team_page = true
  end

  def article_one_page
    @article_one_page = true
  end

  def updates_page
    @updates_page = true
  end
  
end