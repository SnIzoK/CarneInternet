$document.on 'click', '.tab-button', ->

  #     T A B     B U T T O N S
  tab_button = $(this)
  tab_buttons_wrapper = tab_button.closest('.tab-buttons')

  tab_button_index = tab_button.index()

  #     T A B     C O N T E N T
  tab_content_wrapper = $(this).closest('.tabs-content')
  tab_content = tab_content_wrapper.find('.tab-content-wrapper').children()

  #     P E R F O R M     A C T I O N S

  tab_buttons_wrapper.children().removeClass('active')
  tab_button.addClass('active')

  tab_content.filter('.active').removeClass('active')
  next = tab_content.eq(tab_button_index)
  next.addClass('active')